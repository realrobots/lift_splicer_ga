
#include <EEPROM.h>

#ifdef ENABLE_DEBUG
#define DEBUG_ESP_PORT Serial
#define NODEBUG_WEBSOCKETS
#define NDEBUG
#endif

#include <WiFi.h>

#define CURRENT_VERSION 2
String deviceType = "LiftSplicer";

char WIFI_SSID[64] = "";
char WIFI_PASS[64] = "";
#define BAUD_RATE 115200 // Change baudrate to your need

#define PIN_LED_INDICATOR 5

int buttonPins[] = {16, 17, 18, 19, 21};
uint8_t buttonState[] = {0, 0, 0, 0, 0};
unsigned long buttonOnTime[] = {0, 0, 0, 0, 0};

int flashCount = 1;
int currentFlashCount = 0;
long lastFlash = 0;
int longFlashInterval = 3000;
int shortFlashInterval = 300;


void InitWifi()
{
  Serial.print("Connecting to WiFi: ");
  Serial.println(WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASS);

  int failCount = 0;
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.printf(".");
    delay(250);
    failCount++;
    if (failCount >= 20)
    {
      Serial.println("Wifi Connection Failed");
      break;
    }
  }
  if (WiFi.status() == WL_CONNECTED)
  {
    Serial.printf("connected!\r\n[WiFi]: IP-Address is %s\r\n", WiFi.localIP().toString().c_str());
  }
}

// main setup function
void setup()
{
  for (int i = 0; i < 5; i++)
  {
    pinMode(buttonPins[i], OUTPUT);
    digitalWrite(buttonPins[i], 1);
  }
  pinMode(PIN_LED_INDICATOR, OUTPUT);
  digitalWrite(PIN_LED_INDICATOR, 0);

  InitSerial();
  EEPROM.begin(512);
  //SetDefaults();
  GetCredsFromEEPROM();

  InitWifi();
  if (WiFi.status() == WL_CONNECTED)
  {
    InitFirebase();
  }
  else
  {
    Serial.print("Initializing Access Point ");
    Serial.print(GetAPName());
    Serial.println(" for wifi credential entry...");
    // launchWeb();
    setupAP(); // Setup HotSpot
  }
}

void loop()
{

  CheckSerial();

  if (WiFi.status() == WL_CONNECTED)
  {
    UpdateFirebase();
    UpdateButtons();
  }
  else
  {
    ServerLoop();
    FlashIndicator();
  }
}

void PressButton(int idx)
{
  Serial.print("Button Pressed: ");
  Serial.println(idx);
  digitalWrite(buttonPins[idx], 0);
  digitalWrite(PIN_LED_INDICATOR, 1);
  buttonOnTime[idx] = millis();
  buttonState[idx] = 1;
}

void UpdateButtons()
{
  for (int i = 0; i < 5; i++)
  {
    if (buttonState[i])
    {
      if (millis() - buttonOnTime[i] > 200)
      {
        Serial.print("Button Released: ");
        Serial.println(i);
        digitalWrite(buttonPins[i], 1);
        digitalWrite(PIN_LED_INDICATOR, 0);
        buttonState[i] = 0;
      }
    }
  }
}

void SetIndicatorFlashCount(int count)
{
  flashCount = count;
  currentFlashCount = 0;
  digitalWrite(PIN_LED_INDICATOR, LOW);
}

void FlashIndicator()
{
  if (currentFlashCount < flashCount)
  {
    if (millis() - lastFlash > shortFlashInterval)
    {
      digitalWrite(PIN_LED_INDICATOR, !digitalRead(PIN_LED_INDICATOR));
      lastFlash = millis();

      if (!digitalRead(PIN_LED_INDICATOR))
      {
        currentFlashCount++;
      }
    }
  }
  else
  {
    if (millis() - lastFlash > longFlashInterval)
    {
      digitalWrite(PIN_LED_INDICATOR, !digitalRead(PIN_LED_INDICATOR));
      lastFlash = millis();
      currentFlashCount = 0;
    }
  }
}

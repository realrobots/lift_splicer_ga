#include <Firebase_ESP_Client.h>

// Provide the RTDB payload printing info and other helper functions.
#include <addons/RTDBHelper.h>

#define DATABASE_URL "https://reallift-502df-default-rtdb.firebaseio.com/" //<databaseName>.firebaseio.com or <databaseName>.<region>.firebasedatabase.app
#define DATABASE_SECRET "MrnewdP7mkeDHJiIP9LNjXysA7ITK7pcHUcWMPYE"

/* 3. Define the Firebase Data object */
FirebaseData fbdo;

/* 4, Define the FirebaseAuth data for authentication data */
FirebaseAuth auth;

/* Define the FirebaseConfig data for config data */
FirebaseConfig config;

unsigned long dataMillis = 0;
int count = 0;

unsigned long lastDBCheck = 0;
int dbCheckInterval = 500;

// Used as deviceID in database
String macAddress;

void InitFirebase()
{
    Serial.printf("Firebase Client v%s\n\n", FIREBASE_CLIENT_VERSION);
    config.database_url = DATABASE_URL;
    config.signer.tokens.legacy_token = DATABASE_SECRET;

    Firebase.reconnectWiFi(true);

    /* Initialize the library with the Firebase authen and config */
    Firebase.begin(&config, &auth);

    macAddress = WiFi.macAddress();
    InitDeviceRecord();
}

void InitDeviceRecord()
{
    Firebase.RTDB.setString(&fbdo, "/devices/" + macAddress + "/deviceId", macAddress);
    delay(20);
    Firebase.RTDB.setString(&fbdo, "/devices/" + macAddress + "/deviceType", deviceType);
    delay(20);
    Firebase.RTDB.setInt(&fbdo, "/devices/" + macAddress + "/isOn", 1);
    delay(20);
    Firebase.RTDB.setInt(&fbdo, "/devices/" + macAddress + "/currentVersion", CURRENT_VERSION);
    delay(20);
    Firebase.RTDB.setInt(&fbdo, "/devices/" + macAddress + "/targetFloor", -1);
    delay(20);
    Firebase.RTDB.setInt(&fbdo, "/devices/" + macAddress + "/tryUpdate", 0);
    delay(500);
}

void UpdateFirebase()
{
    if (millis() - lastDBCheck > dbCheckInterval)
    {
        lastDBCheck = millis();
        GetDBState();
    }
}

void GetDBState()
{
    int tryUpdate = 0;

    if (Firebase.RTDB.getInt(&fbdo, "/devices/" + macAddress + "/tryUpdate"))
    {
        if (fbdo.dataType() == "int")
        {
            tryUpdate = fbdo.intData();
            if (tryUpdate){
                Firebase.RTDB.setInt(&fbdo, "/devices/" + macAddress + "/tryUpdate", 0);
                Serial.println("Looking for updates");
                CheckForUpdate();
            }
        }
    }
    else
    {
        Serial.println(fbdo.errorReason());
    }


    int targetFloor = -1;

    if (Firebase.RTDB.getInt(&fbdo, "/devices/" + macAddress + "/targetFloor"))
    {
        if (fbdo.dataType() == "int")
        {
            targetFloor = fbdo.intData();
            // Serial.println(targetFloor);
        }
    }
    else
    {
        Serial.println(fbdo.errorReason());
    }
    if (targetFloor != -1)
    {
        PressButton(targetFloor);
        Firebase.RTDB.setInt(&fbdo, "/devices/" + macAddress + "/targetFloor", -1);
    }
}
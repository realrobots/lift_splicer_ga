#define COMMAND_SIZE 256
static char data[COMMAND_SIZE];
static int serial_count;

static int c;

void InitSerial()
{
  Serial.begin(115200);
  
  PrintInstructions();
}

void PrintInstructions()
{
  Serial.println("RealRobots Lift Controller");
  Serial.println("Google Assistant");
  Serial.print("DeviceID: ");
  Serial.println(WiFi.macAddress());
  Serial.print("Version ");
  Serial.println(CURRENT_VERSION);
  Serial.println("");
  Serial.println("To set up credentials:");
  Serial.println("0=SSID");
  Serial.println("1=PASSWORD");
  Serial.println("eg to set PASSWORD. 1=mypassword");
  Serial.println("9 UPDATE FIRMWARE");
  Serial.println("? Print these instructions");
  Serial.println();
  Serial.println("r to restart");
}

void CheckSerial()
{
  while (Serial.available() > 0)
  {
    c = Serial.read();
    if (serial_count < COMMAND_SIZE)
    {
      //      Serial.print(serial_count);
      //      Serial.print('\t');
      //      Serial.println(c);
      data[serial_count] = c;
      serial_count++;
    }
  }

  // Incoming packet should be at least one byte plus the EOL \r\n
  if (serial_count > 2)
  {
    // Serial.println(serial_count);
    if (data[serial_count - 2] == '\r' && data[serial_count - 1] == '\n')
    {
      InterpretData();
    }
    // ClearData();
  }
  else
  {
    // ClearData();
  }
}

void InterpretData()
{
  Serial.println("Trying to interpret");
  Serial.println(data);
  String temp = "";

  if (data[0] == '0' && data[1] == '=')
  {
    Serial.println("Receiving new SSID");
    for (int i = 2; i < serial_count - 2; i++)
    {
      temp += data[i];
    }
    Serial.println(temp);
    StoreNewSSID(temp);
  }
  else if (data[0] == '1' && data[1] == '=')
  {
    Serial.println("Receiving new WiFI password");
    for (int i = 2; i < serial_count - 2; i++)
    {
      temp += data[i];
    }
    Serial.println(temp);
    StoreNewPass(temp);
  }
  else if (data[0] == '9')
  {
    Serial.println("Attempting to download firmware update");
    CheckForUpdate();
  }
  else if (data[0] == '?')
  {
    PrintInstructions();
  }
  else if (data[0] == 'r')
  {
    ESP.restart();
  }

  ClearData();
}

void ClearData()
{
  for (int i = 0; i < COMMAND_SIZE; i++)
  {
    data[i] = 0;
  }
  serial_count = 0;
}


void SetDefaults() {
  char store_ssid[64] = "";
  for (int i = 0; i < 64; i++) {
    EEPROM.write(0 + i, store_ssid[i]);
  }

  char store_pwd[64] = "";
  for (int i = 0; i < 64; i++) {
    EEPROM.write(64 + i, store_pwd[i]);
  }
  

  EEPROM.commit();
}

void StoreNewSSID(String newString) {
  for (int i = 0; i < 64; i++) {
    EEPROM.write(0 + i, newString[i]);
  }
  Serial.println("Stored: " + newString);
  EEPROM.commit();
}

void StoreNewPass(String newString) {
  for (int i = 0; i < 64; i++) {
    EEPROM.write(64 + i, newString[i]);
  }
  Serial.println("Stored: " + newString);
  EEPROM.commit();
}


void GetCredsFromEEPROM() {
  for (int i = 0; i < 64; i++) {
    WIFI_SSID[i] = char(EEPROM.read(0 + i));
  }
  for (int i = 0; i < 64; i++) {
    WIFI_PASS[i] = char(EEPROM.read(64 + i));
  }  
}
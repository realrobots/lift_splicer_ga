# RealRobots Lift Splicer Board

This board is designed to plug into the input socket of an SB200M board.
It acts as a web server and google voice receiver for remote control of the lift.

## Installation

### Connect to local wifi

After powering the board with 5VDC through the screw terminal it will try to connect to the local wifi.

If it doesn't know the correct credentials it will instead act as a wifi access point named "RealRobotsLift"

Connect to that wifi hotspotusing a phone or computer and use a browser to navigate to "192.168.4.1"

NOTE: It may be necessary to turn off mobile data to navigate to the local address.

The web page will show a list of available wifi connections.

Make sure to note down the MAC address at the top of the page as you'll need this to register your device with Google Assistant.

Type in the name and password of your local wifi and press submit.

Press the reset button on the RESET / SW1 button to reboot.

#### Link device to google account 

Google account needs to be added to the list of test accounts by Jake (we're stuck with this until development is a bit further along and I can get approval to be added to Google Assistant properly)

Go to https://console.actions.google.com/

login to your google account, choose "RealLift", press the "TEST" tab and try the "Talk to Lift" command.

The first thing the test environment will do is ask you to link your google account, say yes, then yes again. After that you should be able to use it from here or directly on your phone.

NOTE: Google account needs to be linked to RealLift before this will work

Navigate to https://us-central1-reallift-502df.cloudfunctions.net/login

Log in using the same google account that is linked to RealLift and used by Google Assistant

Type the MAC address (eg. 32:F2:91:00:2B:9D) of your device into the DeviceID field and press submit.

The indicator light on the board should flash briefly when a button press is simulated.


## How to use
Once set up simply activate the Google Assistant and say "Talk to lift"
Then you can use the following commands:

- Press Button One
- Press Button Two
- Press Button Three
- Update
